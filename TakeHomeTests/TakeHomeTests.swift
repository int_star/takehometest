//
//  TakeHomeTests.swift
//  TakeHomeTests
//

import XCTest
@testable import TakeHome

class TakeHomeTests: XCTestCase {
    
    let TIMEOUT: NSTimeInterval = 360;
    let converter:ContentConverter = ContentConverter();
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testConversion() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let path = bundle.pathForResource("tests", ofType: "json")
        let data = NSData(contentsOfFile: path!)
        if let json: NSDictionary = try? NSJSONSerialization.JSONObjectWithData(data!,
                                                                                options: .MutableContainers)
            as! NSDictionary {
            if let tests: NSArray = json["tests"] as? NSArray {
                for test in tests {
                    runTest(test as! [String : AnyObject])
                }
            }
            
        }
    }
    
    func runTest(test: [String: AnyObject]) {
        let expectation = expectationWithDescription("testing")
        let input = test["input"] as! String
        self.converter.convertToJSON(chatString: input) { (jsonString, json, error) -> (Void) in
            print(jsonString)
            let isValid = self.validate(test["output"] as! NSDictionary, result: json!)
            print(isValid)
            XCTAssertTrue(isValid)
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(TIMEOUT) { (error) in
            XCTAssertNil(error, "Expectation timeout with error \(error)")
        }
    }
    
    func validate(expected: NSDictionary, result:NSDictionary) -> Bool {
        return expected.isEqualToDictionary(result as [NSObject : AnyObject])
    }
    
    
    
    
}
