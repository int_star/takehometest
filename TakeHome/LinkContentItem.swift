//
//  LinkContentItem.swift
//  TakeHome
//

import Foundation

typealias FetchUrlContentCompletion = (
    data: NSData?, response: NSURLResponse?, error: NSError?
) -> Void

/**
 Content item representing links
 **/
class LinkContentItem: ContentItem {

    override func processContent(completion completion: ContentPreparationCompletion) {
        
        let group = dispatch_group_create()
        var linksInfo = [AnyObject]()
        for urlString in self.items {
            dispatch_group_enter(group)
            self.fetchUrlContent(url: NSURL(string: urlString)!) { [unowned self] (data, response, error) in
                guard error == nil else {
                    return
                }
                if let title = self.findTitle(data: data, response: response) {
                    linksInfo.append(["url": urlString, "title": title])
                } else {
                    linksInfo.append(["url": urlString])
                }
                dispatch_group_leave(group)
            }
        }
        
        dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            completion(dictionary: [self.category: linksInfo])
        }
    }
    
    private func fetchUrlContent(url url:NSURL, completion: FetchUrlContentCompletion) {
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPShouldHandleCookies = false
        request.HTTPMethod = "GET"
        let session = NSURLSession.sharedSession()
        session.dataTaskWithRequest(request) { (data, response, error) in
            completion(data: data, response: response, error: error)
        }.resume();
    }
    
    func findTitle(data data:NSData?, response: NSURLResponse?) -> String? {
        guard data != nil else {
            return nil
        }
        
        let titleRegex = "<title>(.*?)</title>"
        let html: NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
        let regex: NSRegularExpression? = try? NSRegularExpression(pattern: titleRegex, options: [])
        if let match = regex?.firstMatchInString(html as String, options: [], range: NSMakeRange(0, html.length)) {
            var matchedString = html.substringWithRange(match.range)
            matchedString = matchedString.stringByReplacingOccurrencesOfString("<title>", withString: "" )
            matchedString = matchedString.stringByReplacingOccurrencesOfString("</title>", withString: "")
            return matchedString
        } else {
            return nil
        }
    }
}