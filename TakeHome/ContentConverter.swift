//
//  ContentConverter.swift
//  TakeHome
//

import Foundation

typealias ContentConversionCompletion = (
    jsonString: NSString?,
    json: NSDictionary?,
    error: NSError?
) -> (Void)

typealias Categories = [String: ContentItem]

let PREFIX_MENTIONS = "@"
let PREFIX_EMOTICONS = "("
let PREFIX_HTTP = "http"
let PREFIX_WWW = "www"

let KEY_MENTIONS = "mentions"
let KEY_EMOTICONS = "emoticons"
let KEY_LINKS = "links"

/**
 Class responsible to convert chat message to JSON representation.  
 **/
class ContentConverter {

    /**
     Converts a given chatstring to JSON
     **/
    func convertToJSON(chatString chatString: String,
                                  completion: ContentConversionCompletion) {
        
        let REGEX_MENTIONS = "(^|\\s)@($|\\w+)"
        let REGEX_EMOTICONS = "(\\()(\\w{1,15})\\)"
        let REGEX_LINKS = "(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))"
        
        let matches = find(withRegexPatterns: [REGEX_MENTIONS, REGEX_EMOTICONS, REGEX_LINKS],
                           string: chatString)
        let allCategories = categorize(matches: matches, inString: chatString)
        self.normalize(categories: allCategories, completion: completion)
    }
    
    /**
     Apply patterns to filter the interested content.
     Optionally patterns can be injected during testing
    **/
    func find(withRegexPatterns patterns: [String], string: String) -> [NSTextCheckingResult] {
        let regexPattern = patterns.joinWithSeparator("|")
        let regex = try? NSRegularExpression(pattern: regexPattern, options: [])
        let matches = regex!.matchesInString(string, options: [], range: NSMakeRange(0, string.characters.count));
        return matches
    }
    
    /**
     Categorizes the matches so that we operate on defined ContentItem(s)
    **/
    func categorize(matches matches: [NSTextCheckingResult],
                      inString: NSString) -> Categories {
        
        var allCategories = Categories()
        for match in matches {
            var matchedString = inString.substringWithRange(match.range)
            matchedString = matchedString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            var category: String?
            
            var item: ContentItem?
            if matchedString.hasPrefix(PREFIX_MENTIONS) {
                category = KEY_MENTIONS
                matchedString = matchedString.substringFromIndex(matchedString.startIndex.successor())
            } else if matchedString.hasPrefix(PREFIX_EMOTICONS) {
                category = KEY_EMOTICONS
                matchedString = matchedString[matchedString.startIndex.successor() ..< matchedString.endIndex.predecessor()]
            } else if matchedString.hasPrefix(PREFIX_HTTP) || matchedString.hasPrefix(PREFIX_WWW) {
                category = KEY_LINKS
                item = allCategories[category!] ?? LinkContentItem(category: category!)
                allCategories[category!] = item
            }
            
            if category != nil && !matchedString.isEmpty {
                item = allCategories[category!]
                item = item ?? ContentItem(category: category!)
                item?.add(item: matchedString)
                allCategories[category!] = item
            }
            
        }
        
        return allCategories
    }
    
    /**
     Normalizes the ContentItem(s) to JSON representation.
    **/
    func normalize(categories categories: Categories, completion: ContentConversionCompletion) {

        let group = dispatch_group_create()
        var normalized = [String: AnyObject]()
        
        for (category, item) in categories {
            dispatch_group_enter(group)
            item.processContent { (dictionary) in
                if dictionary != nil {
                    normalized[category] = dictionary![category]
                }
                dispatch_group_leave(group)
            }
        }
        
        dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(normalized, options: NSJSONWritingOptions.PrettyPrinted)
                var prettyJSON: NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                prettyJSON = prettyJSON.stringByReplacingOccurrencesOfString("\\/", withString: "/")
                let json: NSDictionary = try! NSJSONSerialization.JSONObjectWithData(jsonData,
                                                                                     options: .AllowFragments)                    as! NSDictionary
                dispatch_async(dispatch_get_main_queue()) {
                    completion(jsonString: prettyJSON, json: json, error: nil)
                }
            } catch let error as NSError {
                dispatch_async(dispatch_get_main_queue()) {
                    completion(jsonString: nil, json: nil, error: error)
                }
            }
        }
    }
    
}
