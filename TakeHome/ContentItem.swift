//
//  ContentItem.swift
//  TakeHome
//

import Foundation

typealias ContentPreparationCompletion = (
    dictionary: [String: AnyObject]?
) -> Void

/**
 A class representing a single content item.
 This holds a category and a list of items.
 It is aware of how to process content and return a dictionary representation.
 **/
class ContentItem {
    var category: String
    var items: [String]
    
    init(category: String) {
        self.category = category
        self.items = [String]()
    }
    
    func add(item item: String) {
        self.items.append(item)
    }
    
    func processContent(completion completion: ContentPreparationCompletion) {
        guard !self.items.isEmpty else {
            completion(dictionary: nil)
            return
        }
        
        let dictionary = [self.category: self.items]
        completion(dictionary: dictionary)
    }
}
