//
//  ViewController.swift
//  TakeHome
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var outputTextView: UITextView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    let converter = ContentConverter();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicator.stopAnimating()
        self.inputTextView.text = "@sriks @john hipchat emoticons are (awesome) https://www.hipchat.com"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onConvertTapped(sender: AnyObject) {
        self.view.endEditing(true)
        self.indicator.startAnimating()
        self.converter.convertToJSON(chatString: self.inputTextView.text) { (jsonString, json, error) -> (Void) in
            self.indicator.stopAnimating()
            self.outputTextView.text = jsonString as! String;
        }
    }
}

